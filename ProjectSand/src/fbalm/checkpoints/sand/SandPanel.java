package fbalm.checkpoints.sand;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class SandPanel extends JPanel {
    
    
    //Actual grid that holds the displayed simulation
    private SandGrid grid;
    //Predefined drawing colors
    private Color[] drawingColors;
    private Timer timer;
    public static final String[] TEST_TYPE = new String []{
            "Critical value","Stickiness"
    };
    
    public SandPanel(int criticalValue)
    {
        grid = new SandGrid(25,25,criticalValue,60000L);
        resetColors();
        timer = new Timer(40, (e)-> { 
            //As long as the grid is running, keep going
            //To prevent confusion, stop the timer when end
            //is reached
            if(grid.isRunning()){
                grid.run(1);
                repaint();
            } else {
                ((Timer)e.getSource()).stop();
            }
        });
    }
    /**
     * Set the colors appropriate to the critical value of the sand in the grid
     */
    public void resetColors()
    {
        //Maximum dependent on 
        int criticalValue = grid.getCriticalValue();
        drawingColors = new Color[criticalValue];
        //Since the critical value is never achieved, we need 1 color for each
        for(int i = 0; i<criticalValue; ++i)
        {
            //The saturation goes from 0 to 1 depending on the amount of sand
            float saturation = (float) i * (1.0f / criticalValue);
            //Hue of 0 corresponds to red
            drawingColors[i] = Color.getHSBColor(0.05f, saturation, 0.9f);
        }
        repaint();
    }
    
    /**
     * Start or stop the timer depending on whether it is running or not
     * @return If the timer has been started now
     */
    public void toggleTimer()
    {
        if(!timer.isRunning())
        {
            timer.start();
        } else {
            timer.stop();
        }
    }
    
    
    /**
     * Reset the grid to a newly randomized value.
     * @throws NumberFormatException Incorrect format in the input
     */
    public void resetGrid() throws NumberFormatException
    {
        timer.stop();
        grid.reset();
        resetColors();
    }
    /**
     * 
     * @throws NumberFormatException Incorrect format in the input
     */
    public void changeGridSize() throws NumberFormatException
    {
        grid.changeSize();
    }
    public void emptyGrid()
    {
        grid.emptyGrid();
        repaint();
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        //Paint the background
        super.paintComponent(g);
        
        for(int i = 0; i<grid.getWidth(); i++)
        {
            for(int j = 0; j<grid.getHeight(); ++j)
            {
                //Height of each rectangle depends on the current width of the panel and 
                //the number of rectangles to fill it with.
                double rectangleHeight = (double)getHeight()/(double)grid.getHeight();
                double rectangleWidth = (double)getWidth()/(double)grid.getWidth();
                
                Rectangle2D rect = new Rectangle2D.Double(
                                            i*rectangleWidth, 
                                            j*rectangleHeight, 
                                            rectangleWidth,
                                            rectangleHeight);
                
                int numberOfGrains = grid.get(i,j);
                //White is a more clear empty color than the default 0-grains color
                g.setColor(numberOfGrains > 0 ? (drawingColors[numberOfGrains]): Color.WHITE);
                g2d.fill(rect);
            }
        }
    }
    
    public void runTest(String testType)
    {
        //Get input from user via a number of spinners. The spinners are
        //limited to prevent calculation time from becoming exceedingly long
        
        JSpinner widthSpinner = new JSpinner(
                new SpinnerNumberModel(25,1,200,1));
        JSpinner heightSpinner = new JSpinner(
                new SpinnerNumberModel(25,1,200,1));
        
        //Range of critical values to explore, inclusive on either end
        JSpinner testTypeMinimumSpinner = new JSpinner(
                new SpinnerNumberModel(5,5,50,1));
        JSpinner testTypeMaximumSpinner = new JSpinner(
                new SpinnerNumberModel(5,5,500,1));
        JSpinner iterationLimitSpinner = new JSpinner(
                new SpinnerNumberModel(10000L, 10000L,1000000L,1000L));
        
        JRadioButton timeButton = new JRadioButton("Time");
        JRadioButton sizeButton = new JRadioButton("Size");
        //Button group to have only one of the buttons selected
        ButtonGroup group = new ButtonGroup();
        group.add(sizeButton);
        group.add(timeButton);
        //enabled by default:
        sizeButton.setSelected(true);
        
        //Array to display the components in the option dialog
        final JComponent[] inputs = new JComponent[] {
                new JLabel("Number of cells in width:"),
                widthSpinner,
                new JLabel("Number of cells in height:"),
                heightSpinner,
                new JLabel("Minimum" + testType + "value"),
                testTypeMinimumSpinner,
                new JLabel("Maximum critical value: "),
                testTypeMaximumSpinner,
                new JLabel("Minimum clumpiness factor"),
                iterationLimitSpinner,
                new JLabel("Property to plot: "),
                sizeButton,
                timeButton
        };
        for (JComponent spinner : inputs)
        {
            spinner.setPreferredSize(new Dimension(50,20));
        }
        int response = JOptionPane.showConfirmDialog(
                null,                             //Parent component
                inputs,                           //Components to display
                "Parameters for the test.",       //Title
                JOptionPane.OK_CANCEL_OPTION,   //Action buttons to display
                JOptionPane.PLAIN_MESSAGE,      //Message type
                null);                           //Icon
        if(response != JOptionPane.OK_OPTION)
        {
            return;
        }
        
        int testTypeMinimum = Math.min(
                (int) testTypeMinimumSpinner.getValue(),
                (int) testTypeMaximumSpinner.getValue());
        
        int testTypeMaximum = Math.max(
                (int) testTypeMinimumSpinner.getValue(),
                (int) testTypeMaximumSpinner.getValue());
        long iterationLimit = ((SpinnerNumberModel) iterationLimitSpinner.getModel()).getNumber().longValue();
        
        //Holds all threads and grids for later use
        List<Thread> threads = new ArrayList<>();
        List<SandGrid> grids = new ArrayList<>();
        
        long start = System.nanoTime();
        
        for(int k = testTypeMinimum; k<=testTypeMaximum; k++)
        {
            if(testType == TEST_TYPE[0])
            {
                
            }
            SandGrid g = new SandGrid((int)widthSpinner.getValue(), (int)heightSpinner.getValue(), k, iterationLimit);
            
            Thread t = new Thread(()->{
                while (g.isRunning() && !Thread.currentThread().isInterrupted())
                {
                    //TODO
                    g.run(clumpiness);
                }
            });
            grids.add(g);
            threads.add(t);
            t.start();
        }
        //Wait for each thread to finish
        final long minuteInMillis = 60000L;
        
        threads.stream().parallel()
               .forEach((t) -> {
                   try {
                       t.join(minuteInMillis);
                   } catch (InterruptedException e)
                   {
                       JOptionPane.showMessageDialog(null, "Error", "Could not finish computation within alotted time.", JOptionPane.ERROR_MESSAGE);
                       return;
                   }
        });
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        
        
        List<Double> slopes = new ArrayList<>();
        
        grids.stream()
             .forEach((g)->{
                //Use a simple linear regression to get an estimate for alpha
                SimpleRegression regression = new SimpleRegression(true);
                
                Map<Integer, Long> data;
                //The data collected changes on which button is selected
                data = timeButton.isSelected() ? g.getTimeData() : g.getSizeData();
                //Label for in the legend
                XYSeries series = new XYSeries("k = " + g.getCriticalValue());
                //Process each data point into both the regression and the series as a log-log plot
                data.forEach((k,v) -> {
                    double xValue = Math.log(k.doubleValue());
                    double yValue = Math.log(v.doubleValue());
                    series.add(xValue, yValue);
                    regression.addData(xValue, yValue);
                });
                
                dataset.addSeries(series);
                slopes.add(regression.getSlope());
            });
       
        
        long end = System.nanoTime();
        System.out.println("Time taken: " +  ((double)end - (double)start) / 1000000000.0);
        
        //Get the average value of alpha from all calculated slopes
        
        double averageSlope = slopes.stream()
                                      .mapToDouble((x)->x)
                                      .average()
                                      .getAsDouble();
        
        System.out.printf("Average slope of the log-log plot: %.3f\n", averageSlope);
        
        String titleString = "log of frequency of avalanches against their " + 
                                    (timeButton.isSelected() ? "durations" : "sizes");
        String xAxisString = timeButton.isSelected() ? 
                                    "Log of duration of avalanche" : "Log of size of avalanche";
        String yAxisString = "Log of frequency";
        
        //Create a chart from the XY data
        displayXYChart(dataset, titleString, xAxisString,yAxisString);
        
        
    }
    
    
    public void displayXYChart(XYSeriesCollection dataset, String title, String xAxis, String yAxis)
    {
        JFreeChart chart = ChartFactory.createXYLineChart(title,
                xAxis,
                yAxis,
                dataset,
                PlotOrientation.VERTICAL,
                true, //Do have a legend
                true,  //Axis labels
                false);
        ChartFrame chartFrame = new ChartFrame("Histogram",chart);
        
        chartFrame.setVisible(true);
        chartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        chartFrame.setSize(800,600);
        
        //Allow for the suer to save the histogram when closing the chart
        chartFrame.addWindowListener(new WindowAdapter(){
            
            @Override
            public void windowClosing(WindowEvent ev){
                
                //Chart might want to be saved
                int response = JOptionPane.showConfirmDialog(
                        null, 
                        "Do you wish to save the chart?",
                        "Save",
                        JOptionPane.YES_NO_OPTION);
                
                if(response == JOptionPane.YES_OPTION)
                {
                    //Filter on JPEG/JPG only
                    FileFilter filter = 
                            new FileNameExtensionFilter(
                                    ".jpg",
                                     new String[]{"jpg", "jpeg"});
                    JFileChooser fc = new JFileChooser();
                    fc.setFileFilter(filter);
                    fc.addChoosableFileFilter(filter);
                    //Set default directory to root
                    fc.setCurrentDirectory(new File("/"));
                  
                    //If a file has been selected
                    int saveResponse = fc.showSaveDialog(null);
                    if(saveResponse == JFileChooser.APPROVE_OPTION)
                    {
                        File selectedFile = fc.getSelectedFile();
                        String filePath = selectedFile.getAbsolutePath();
                        
                        //If it doesn't match with .jpeg/.jpg at the end
                        if(!filePath.matches(".*\\.(jpeg|jpg)$"))
                        {
                            //If it currently has an extension after the final separator
                            if(filePath.contains(".") &&
                                 (filePath.lastIndexOf(File.separator) < filePath.lastIndexOf(".")))
                            {
                                //Cut the file path off before the extension point
                                filePath = filePath.substring(0,filePath.lastIndexOf("."));
                            }
                            //Add .jpg as an extension
                            filePath += ".jpg";
                            //Change the file to the new path
                            selectedFile = new File(filePath);
                        }
                        try 
                        {
                            //Write the chart out as a jpeg
                            ChartUtilities.saveChartAsJPEG(selectedFile, chart, 600, 400);
                        } catch (IOException ex) {
                            //Error message
                            JOptionPane.showMessageDialog(
                                    null, 
                                    "Could not save chart", 
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        
                    }
                }
            }
        });
    }

}

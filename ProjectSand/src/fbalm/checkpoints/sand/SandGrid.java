package fbalm.checkpoints.sand;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JOptionPane;

public class SandGrid {
    private static Random generator;
    static
    {
        generator = new Random(System.nanoTime());
    }
    
    private int[][] currentGrid;
    
    private int gridHeight, gridWidth;
    private int criticalValue;
    private long iterationLimit;
    private long currentStep;
    
    private Map<Integer, Long> timeHistogram;
    private Map<Integer, Long> sizeHistogram;
    
    public SandGrid(int width,int height,int criticalValue, long iterationLimit)
    {
        this.gridWidth = width;
        this.gridHeight = height;
        this.criticalValue = criticalValue;
        this.iterationLimit = iterationLimit;
        this.currentStep = 0L;
        timeHistogram = new HashMap<>();
        sizeHistogram = new HashMap<>();
        currentGrid = new int[width+2][height+2];
        fillGrid(criticalValue);
    }
    
    public SandGrid(SandGrid other)
    {
        
    }
    public long getCurrentStep()
    {
        return currentStep;
    }
    public boolean isRunning()
    {
        return (currentStep < iterationLimit);
    }
    
    public void setLimit(long l)
    {
        currentStep = 0;
        iterationLimit = l;
    }
    
    public Map<Integer,Long> getTimeData()
    {
        return new HashMap<>(timeHistogram);
    }
    
    public Map<Integer,Long> getSizeData()
    {
        return new HashMap<>(sizeHistogram);
    }
    
    public void print()
    {
        for(int[] row : currentGrid)
        {
            System.out.println(Arrays.toString(row));
        }
    }

    public void run(int clumpiness)
    {
        
        int avalancheSize = 0;
        
        addGrain(clumpiness);
        
        int numberOfAvalanches = 1;
        int avalancheTime = 0;
        while (numberOfAvalanches != 0){
            numberOfAvalanches = 0;
            
            for(int x = 1; x<=gridWidth; ++x)
            {
                for(int y = 1; y <= gridHeight; ++y)
                {
                    while(currentGrid[x][y] >= criticalValue)
                    {
                        currentGrid[x][y] -= 4;
                        
                        currentGrid[x-1][y]++;
                        currentGrid[x+1][y]++;
                        currentGrid[x][y-1]++;
                        currentGrid[x][y+1]++;
                        
                        ++numberOfAvalanches;
                        ++avalancheSize;
                    }
                }
            }
            
            ++avalancheTime;
            
        }
        
        if(avalancheSize > 0)
        {
            long sizeCount = sizeHistogram.containsKey(avalancheSize) ? sizeHistogram.get(avalancheSize) : 0L;
            sizeHistogram.put(avalancheSize, sizeCount+1);
        }
        
        if(avalancheTime > 0)
        {
            long timeCount = timeHistogram.containsKey(avalancheTime) ? timeHistogram.get(avalancheTime) : 0L;
            timeHistogram.put(avalancheTime, timeCount+1);
        }
        currentStep++;
    }
    
    public void fillGrid(int criticalValue)
    {
        this.criticalValue = criticalValue;
        currentStep = 0;
        for(int x = 0; x<currentGrid.length; ++x)
        {
            for(int y = 0; y<currentGrid[x].length; ++y)
            {
                currentGrid[x][y] = generator.nextInt(criticalValue);
            }
        }
    }
    
    //Read in values from 
    public void reset() throws NumberFormatException
    {
        String critical = JOptionPane.showInputDialog(null,"Input a value for the critical number in the simulation");
        String iterations = JOptionPane.showInputDialog(null,"Enter a value for the number of iterations");
        if(critical != null && iterations != null)
        {
            criticalValue = Integer.parseInt(critical);
            iterationLimit = Long.parseLong(iterations);
            fillGrid(criticalValue);
            setLimit(iterationLimit);
        }
    }
    
    public void changeSize() throws NumberFormatException
    {
        String width = JOptionPane.showInputDialog(null,"Input a value for the number of columns of cells.");
        String height = JOptionPane.showInputDialog(null,"Enter a value for the number of rows of cells.");
        
        if(width != null && height != null)
        {
            try {
                int newHeight = Integer.parseInt(height);
                int newWidth = Integer.parseInt(width);
                setSize(newHeight, newWidth);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Error in reading numbers, please try again");
            }
        }
    }
    
    public void emptyGrid()
    {
        currentGrid = new int[gridWidth+2][gridHeight+2];
    }

    
    public void setSize(int x, int y)
    {
        currentGrid = new int[x+2][y+2];
        gridWidth = x;
        gridHeight = y;
        
        fillGrid(criticalValue);
    }
    public int getCriticalValue()
    {
        return criticalValue;
    }
    public int get(int x, int y)
    {
        return currentGrid[x+1][y+1];
    }
    
    public int getWidth()
    {
        return gridWidth;
    }
    
    public int getHeight()
    {
        return gridHeight;
    }
    
    public void addGrain(int clumpiness)
    {
        
        //The grid position starts from index 1 in the grid
        int xPosition = generator.nextInt(gridWidth) + 1;
        int yPosition = generator.nextInt(gridHeight) + 1;
        
        currentGrid[xPosition][yPosition]+= clumpiness;
       // currentGrid[13][13]++;
    }
}

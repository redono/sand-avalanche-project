package fbalm.checkpoints.sand;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;



public class SandFrame extends JFrame {
    
    private SandPanel panel;
    public SandFrame()
    {
        setVisible(true);
        setSize(800,800);
        setTitle("Self Organised Criticality in sand piles");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Default critical value is 5
        panel = new SandPanel(5);
        setLayout(new BorderLayout());
        add(panel, BorderLayout.CENTER);
        add(new ButtonPanel(), BorderLayout.PAGE_END);
    }
    //Holds UI for running tests and everything
    private class ButtonPanel extends JPanel
    {
        
        ButtonPanel()
        {
            addButton( this, 
                      "Run", 
                      (ev) -> panel.toggleTimer());
            
            addButton(this, 
                      "Reset", 
                      (ev) -> { try {
                          panel.resetGrid();
                      } catch (NumberFormatException e){
                          JOptionPane.showMessageDialog(null, "Error in reading numbers, please try again");
                      }});
            
            addButton(this,
                      "Change size",
                      (ev) -> { try {
                          panel.changeGridSize();
                      } catch (NumberFormatException e){
                          JOptionPane.showMessageDialog(null, "Error in reading numbers, please try again");
                      }});
            
            addButton(this,
                      "Run Test",
                      (ev) -> {
                          String[] options = Arrays.copyOf(SandPanel.TEST_TYPE, SandPanel.TEST_TYPE.length);
                          int retVal = JOptionPane.showOptionDialog(
                                  null, 
                                  "Test type selection", 
                                  "Select the type of simulation to run", 
                                  JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                              null, options, options[0]);
                          if(retVal != JOptionPane.CLOSED_OPTION)
                          {
                             panel.runTest(options[retVal]);
                          }
                      });
            
            addButton(this,
                      "Empty",
                      (ev) -> panel.emptyGrid());
        }
        /**
         * Shortcut to add a multiple of buttons more easily to a component
         * @param comp Component to add the button to
         * @param text Text to be displayed on the button
         * @param listener Listener to handle what happens when the button is clicked
         */
        void addButton(JComponent comp, String text, ActionListener listener)
        {
            JButton button = new JButton(text);
            button.addActionListener(listener);
            comp.add(button);
        }
    }
    
    public static void main(String... args) throws 
            ClassNotFoundException, 
            InstantiationException, 
            IllegalAccessException, 
            UnsupportedLookAndFeelException
    {
        
        //Windows has a nicer look to it than the standard cross-platform look and feel
        //than the usual theme
        for(LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
        {
            if(info.getName().equalsIgnoreCase("Windows"))
            {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
        }
        EventQueue.invokeLater(() -> new SandFrame());
    }
}
